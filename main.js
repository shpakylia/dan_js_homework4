function createNewUser() {
    let firstName = prompt('enter your first name:');
    let lastName = prompt('enter your last name:');
    !firstName ? '' : firstName;
    !lastName ? '' : lastName;
    let newUser  = {
        get firstName(){
          return this.firstName;
        },
        get lastName(){
          return this.lastName;
        },
        setFirstName(_firstName){
            firstName = _firstName;
        },
        setLasttName(_lastName){
            lastName = _lastName;
        },
        getLogin(){
            if(!firstName && !lastName) return '';
            return ((firstName[0] ? firstName[0] : '') + lastName).toLowerCase();
        }

    };
    Object.defineProperty(newUser, 'firstName', {
        configurable: false,
        writable: false
    });
    Object.defineProperty(newUser, 'lastName', {
        configurable: false,
        writable: false
    });
    return newUser;
}

let user = createNewUser();
console.log(user.getLogin());
user.firstName = 'Juli';
user.lastName = 'Shpak';
console.log(user.getLogin());
